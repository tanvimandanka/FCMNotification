package com.example.tanvi.fcmnotification.service

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import android.support.v4.app.NotificationCompat
import com.example.tanvi.fcmnotification.R
import android.support.v4.app.NotificationManagerCompat




class MyFirebaseMessagingService : FirebaseMessagingService() {
    val CHANNEL_ID = "com.example.tanvi.fcmnotification"
    val notificationId = 20

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        val mBuilder = NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle("From:"+remoteMessage.from)
                .setContentText(remoteMessage.notification.body)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
        Log.d("Inside Message", "From: " + remoteMessage.getFrom());
        Log.d("Inside Message", "Notification Message Body: " + remoteMessage.getNotification().getBody());

        val notificationManager = NotificationManagerCompat.from(this)
        notificationManager.notify(notificationId, mBuilder.build())
    }
}